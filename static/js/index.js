
$(".agree-button").click(function(){
	$(".backdrop").fadeOut()
	$(".termsBox").fadeOut()
})

var firstname = "None";
var lastname = "None";
var gender = "None";

$(document).on("change","select#firstname",function(){
	assignValues()
})
$(document).on("change","select#lastname",function(){
	assignValues()
})
$(document).on("change","select#gender",function(){
	assignValues()
})

$(".toggle_form").click(function(){
	if ($(".signup").is(":hidden")) {
		$(".signup").slideDown();
		$(".signin").slideUp();
		$(".toggle_form").html("<u>Already have an account</u>");
	} else {
		$(".signup").slideUp();
		$(".signin").slideDown();
		$(".toggle_form").html("<u>Don't have an account</u>");
	}
})

/*
*  BEWARE OF THE ^
*/
function assignValues(){
	firstname = $("select#firstname").val()
	lastname = $("select#lastname").val()
	gender = $("select#gender").val()
	showObj();
}
function showObj(){
	JSONObj = {
		"firstname":firstname,
		"lastname":lastname,
		"gender":gender,
	}
}
/*
* CONFIDENTIALS
*/
$("#password2").keypress(function(eve){
	if(eve.charCode == 13){
		$(".signup_button").click()
	}
})
$(".signup_button").click(function(){
	pass1 = $("#password1").val()
	pass2 = $("#password2").val()
	firstname = $("#firstname").val()
	lastname = $("#lastname").val()
	if(pass1 != pass2){
		showError("Password didn't matched")
		return 0;
	}
	else if(firstname == 'None' || lastname == "None"){
		showError("Please Choose Your Name Combination");
		return 0;
	}
	$.ajax({
		type:'POST',
		url:'/userworks',
		data:{
			"firstname":firstname,
			"lastname":lastname,
			"password":pass2,
			"action":"create_user",
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			if(data.status == 'usercreated'){
				window.location = window.location.origin + '/home'
			}
			else{
				showError("That Name Combination already exist.")
			}
		},
		error:function(){
			showError("Error Occured")
		}
	})
})
$("#password_login").keypress(function(eve){
	if(eve.charCode == 13){
		$(".login_button").click()
	}
})
$(".login_button").click(function(){
	firstname = $("#firstname").val()
	lastname = $("#lastname").val()

	if(firstname == 'None' || lastname == "None"){
		showError("Please Choose Your Name Combination !")
		return 0;
	}
	$.ajax({
		type:'POST',
		url:'/userworks',
		data:{
			"firstname":firstname,
			"lastname":lastname,
			"action":"authenticate",
			"password":$("#password_login").val(),
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			if(data.authenticated){
				window.location = window.location.origin + "/home"
			}
			else{
				showError("Invalid Name Combination or Password")
			}			
		},
		error:function(){
			showError("Error Occured")
		}
	})
})