var first;
var second;
var loading = false;


var cert;
var wins;
$(document).on("mouseleave",".Couple1",function(){
	$(".score1").children("span").attr("class","glyphicon glyphicon-heart-empty")
})
$(document).on("mouseenter",".Couple1",function(){
	$(".score1").children("span").attr("class","glyphicon glyphicon-heart")
})
$(document).on("mouseleave",".Couple2",function(){
	$(".score2").children("span").attr("class","glyphicon glyphicon-heart-empty")
})
$(document).on("mouseenter",".Couple2",function(){
	$(".score2").children("span").attr("class","glyphicon glyphicon-heart")
})
$("#iamIn").on("change",function(){
	if($("#iamIn").val() != "None"){
		window.location = window.location.origin + "/home/" + $("#iamIn").val();
	}
	else{
		window.location = window.location.origin + "/home";
	}
})
$("#selectYear").on("change",function(eve){
	if($(this).val() != "None"){
		$.ajax({
			type:'POST',
			url:'/getlist',
			data:{
				"action":"getList",
				"year":$(this).val(),
				"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),				
			},
			success:function(data){
				selectBoy = '<select id="selectBoy" class="couples btn btn-danger">';
				selectBoy += '<option value="None">Select Boy</option>';
				males = data.males;
				females = data.females;

				for(i = 0;i < males.length; i++){
					selectBoy += '<option value="'+males[i]+'">'+males[i]+'</option>'
				}
				selectBoy += '</select>'
				selectGirl = '<select id="selectGirl" class="couples btn btn-danger">';
				selectGirl += '<option value="None">Select Girl</option>';
				for(i = 0;i < females.length; i++){
					selectGirl += '<option value="' + females[i] + '">' + females[i] + '</option>';
				}
				selectGirl += '</select>';
				html_code = selectBoy +'<span class="glyphicon glyphicon-heart heartMatcher"></span>'+ selectGirl
				html_code += "<br><br>";
				html_code += '<button id="postCouple" class="btn btn-danger">Post This Couple</button>';
				html_code += "<br><br>";
				html_code += '<button id="randomCouple" class="btn btn-danger"><span class="glyphicon glyphicon-heart"></span> Post Random Couple</button>';
				$("#coupleSelector").html(html_code)
			},
			error:function(){
				showError("Error Occured: Check Your Internet Connection");
			}
		})
	}
})
function changeCouple(couple1_score,couple2_score){
	getCoupleOfYear = document.getElementById("getCoupleOfYear").value;
	console.log(getCoupleOfYear)
	$.ajax({
		type:'POST',
		url:'/getcouple',
		data:{
			"action":"getCouple",
			"year":getCoupleOfYear,
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			cert = data;
			updateCouple(data);
		},
		error:function(){
			showError("Server Connection Lost");
		}
	})
}
function updateData(winner){
	first_couple_id = cert.first.id;
	second_couple_id = cert.second.id;
	wins = winner;
}
function updateCouple(data){
	$(".firstBoy").text(data.first.boy);
	$(".firstGirl").text(data.first.girl);
	$(".secondBoy").text(data.second.boy);
	$(".secondGirl").text(data.second.girl);
	$(".score1").html(data.first.score + ' <span class="glyphicon glyphicon-heart-empty" style="color:#ff0700"></span>');
	$(".score2").html(data.second.score + ' <span class="glyphicon glyphicon-heart-empty" style="color:#ff0700"></span>');
}
$(".createMatch").click(function(){
	$(".backDrop").fadeIn("slow")
	$(".coupleMaker").fadeIn()
})
$(".backDrop").click(function(){
	$(".backDrop").fadeOut("fast")
	$(".coupleMaker").fadeOut()
})
function getCouple(){
	getCoupleOfYear = document.getElementById("getCoupleOfYear").value;
	$.ajax({
		type:'POST',
		url:'/getcouple',
		data:{
			"action":"getCouple",
			"year":getCoupleOfYear,
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			cert = data;
			updateCouple(data);
		},
		error:function(){
			getCouple()

		}
	})
}
getCouple()
$(".Couple1").click(function(){
	getCoupleOfYear = document.getElementById("getCoupleOfYear").value;
	console.log(getCoupleOfYear)
	updateData(1);
	if(loading){
		return 0;
	}
	loading = true;
	$.ajax({
		type:'POST',
		url:'/ratecouple',
		data:{
			"action":"rateCouple",
			"wins":wins,
			"first":first_couple_id,
			"second":second_couple_id,
			"year":getCoupleOfYear,
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			bigLove(data.instance.change);
			updateCouple(data);
			cert = data;
			loading = false;
		},
		error:function(data){
			loading = false;
			showError("Error occured in rating this couple")
		}
	})
})
$(".Couple2").click(function(){
	getCoupleOfYear = document.getElementById("getCoupleOfYear").value;
	console.log(getCoupleOfYear)
	updateData(2)
	if(loading){
		return 0;
	}
	loading = true;
	$.ajax({
		type:'POST', 
		url:'/ratecouple',
		data:{
			"action":"rateCouple",
			"wins":wins,
			"first":first_couple_id,
			"second":second_couple_id,
			"year":getCoupleOfYear,
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			bigLove(data.instance.change)
			updateCouple(data);
			cert = data;
			loading = false;
		},
		error:function(){
			loading = false;
			showError("Click Again :] ")
		}
	})
})
$(document).on("click","#postCouple",function(){
	boy = $("#selectBoy").val();
	girl = $("#selectGirl").val();
	year = $("#selectYear").val();
	if(boy == "None" || girl == "None"){
		showError("No Couple Selected.");
		return 0;
	}
	$.ajax({
		type:'POST',
		url:'/specificnewcouple',
		data:{
			"boy":boy,
			"girl":girl,
			"year":year,
			"action":"specific",
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			if(data.posted){
				showError("Congratulation you Couple is Posted");
				$(".coupleMaker").fadeOut()
			}
			else if(data.posted === false){
				showError("Couple Already Exist")
			}
		},
		error:function(data){
			showError("Error in posting couple");
		}
	})
})
$(document).on("click","#randomCouple",function(){
	$.ajax({
		type:'POST',
		url:'/randomnewcouple',
		data:{
			"action":"random",
			"year":$("#selectYear").val(),
			"csrfmiddlewaretoken":$("input[name=csrfmiddlewaretoken]").val(),
		},
		success:function(data){
			showError("Posted " + data.boy + " and " + data.girl + " of " + data.year);
			$(".coupleMaker").fadeOut()
			$(".backDrop").fadeOut()
		},
		error:function(){
			showError("Error occured in posting couple");
		}
	})
})


function bigLove(score){
	$(".bigLove").fadeIn()
	$(".points").html(score)
	$(".bigLove").animate({
		"top":'50%',
		"left":'50%',
		"margin-left":"-40px",
		"margin-top":"-40px"
	})
	$(".points").fadeIn("fast")
	$(".bigLove").animate({
		"font-size":"500px",
		"margin-left":"-250px",
		"margin-top":"-250px",
	}).fadeOut(2000)
	
	$(".bigLove").css({
		"top":"100%",
		'font-size':'80px',
		'position':'fixed',
		'z-index':'20',
		"color":"darkred",
		"left":"0",
	})
	$(".points").fadeOut(2000)
}


// FOR CHAT SHIT
