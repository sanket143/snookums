"""snookums URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from home import views as homeViews
urlpatterns = [
    path('', homeViews.index),
    path('home/',homeViews.home),
    path('home/<str:year>',homeViews.home),
    path('refresh/<str:what>',homeViews.refresh),
    path('something/<str:keyword>',homeViews.something),
    path('update',homeViews.update),
    path('getcouple',homeViews.getCouple),
    path('ratecouple',homeViews.rateCouple),
    path('randomnewcouple',homeViews.randomNewCouple),
    path('specificnewcouple',homeViews.specificNewCouple),
    path('getlist',homeViews.getList),
    path('updatestudentid',homeViews.updateStudentId),
    path('chat',homeViews.chatUpdate),
    path('newcouple',homeViews.newCouple),
    path('userworks',homeViews.userWorks),
    path('logout',homeViews.logoutView),
    path('hsrshldsnozxenqxntqnkcrhm',homeViews.countHits),
    path('hsrshldsnozx',homeViews.hitsUpdate),
    path('uniques', homeViews.uniques),
    path('thegrid/', admin.site.urls),
]
