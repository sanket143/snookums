# Generated by Django 2.0 on 2018-02-18 17:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_firstname_message_surname'),
    ]

    operations = [
        migrations.AlterField(
            model_name='firstname',
            name='used_with',
            field=models.TextField(default='[]'),
        ),
        migrations.AlterField(
            model_name='surname',
            name='user_with',
            field=models.TextField(default='[]'),
        ),
    ]
