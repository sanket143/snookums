from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.

class Student(models.Model):
	name = models.CharField(max_length=200)
	year = models.IntegerField(default=0)
	gender = models.CharField(max_length=100)
	coupled_with = models.TextField(default="[]")

	def __str__(self):
		return self.name

class Couple(models.Model):
	boy = models.CharField(max_length=200)
	girl = models.CharField(max_length=200)
	year = models.IntegerField(default=0)
	score = models.FloatField(default=1000)
	appeared = models.IntegerField(default=0)
	def __str__(self):
		return self.boy + " and " + self.girl + " > " + str(self.score)

	class Meta:
		ordering = ["-score"]

class FirstName(models.Model):
	name = models.CharField(max_length=100,unique=True)
	used_with = models.TextField(default="[]")
	times_used = models.IntegerField(default=0)

	def __str__(self):
		return self.name

	class Meta:
		ordering = ["-times_used"]

class Surname(models.Model):
	surname = models.CharField(max_length=100,unique=True)
	user_with = models.TextField(default="[]")
	times_used = models.IntegerField(default=0)

	def __str__(self):
		return self.surname

	class Meta:
		ordering = ["-times_used"]

class Message(models.Model):
	user = models.ForeignKey(User, on_delete=models.PROTECT)
	message = models.CharField(max_length=200)
	timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __str__(self):
		return self.user.username + " > " + self.message
class UserInfo(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	student_id = models.IntegerField(default=0)
	def __str__(self):
		return self.user.username + " > " + str(self.student_id)

@receiver(post_save,sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		UserInfo.objects.create(user=instance)


class Hit(models.Model):
	seeker = models.CharField(max_length=100)
	hits = models.IntegerField(default=0)