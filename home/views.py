import requests
import json
import math
from random import randint
from django.shortcuts import render, redirect
from django.http import HttpResponse
from home.models import Student, Couple, FirstName, Surname, Message, Hit, UserInfo
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
import time
# Create your views here.
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def index(request):
	if request.user.is_authenticated:
		return render(request,'loggedin.html')
	names = FirstName.objects.all()
	last_names = Surname.objects.all()
	return render(request,'index.html',{"names":names,"lastnames":last_names})

def home(request,year=None):
	if(len(Hit.objects.all()) == 0):
		Hit.objects.create(seeker='sanket')
	me = Hit.objects.get(seeker='sanket')
	me.hits += 1
	me.save()
	snippets = Message.objects.all()
	years = ['first', 'second', 'third', 'fourth', None]
	if year not in years:
		return redirect("/home")
	return render(request, "home.html",{"snippets":snippets,"year":year})

def result(request):
	return render(request,'result.html')

def logoutView(request):
	logout(request)
	return redirect('/')

def update(request):
	return HttpResponse("haha")

def updateStudentId(request):
	if request.method == 'POST':
		user = User.objects.get(username=request.user.username)
		user.userinfo.student_id = request.POST.get("studentid")
		user.userinfo.save()
		user.save()
		return HttpResponse("Yeah")
	return HttpResponse("404 Not Found")

def getCouple(request):
	if request.method == 'POST':
		me = Hit.objects.get(seeker='sanket')
		me.hits += 1
		me.save()
		if request.POST.get("year") != 'None':
			year = request.POST.get("year")
			if year == 'first':
				yearInt = 2017
			elif year == 'second':
				yearInt = 2016
			elif year == 'third':
				yearInt = 2015
			elif year == 'fourth':
				yearInt = 2014
			else:
				return HttpResponse("No Such Year")
			random_student_one = randint(0,len(Couple.objects.filter(year=yearInt))-1)
			first_couple = Couple.objects.filter(year=yearInt)[random_student_one]
			while(True):
				random_student_two = randint(0,len(Couple.objects.filter(year=yearInt))-1)
				if random_student_two != random_student_one:
					second_couple = Couple.objects.filter(year=yearInt)[random_student_two]
					break;
				time.sleep(0.5)
		else:
			random_student_one = randint(0,len(Couple.objects.all())-1)
			first_couple = Couple.objects.all()[random_student_one]
			while(True):
				random_student_two = randint(0,len(Couple.objects.all())-1)
				if random_student_two != random_student_one:
					second_couple = Couple.objects.all()[random_student_two]
					break;
				time.sleep(1)

		first_couple_obj = {
			"boy":first_couple.boy,
			"girl":first_couple.girl,
			"year":first_couple.year,
			"score":round(first_couple.score,2),
			"appeared":first_couple.appeared,
			"id":first_couple.id,
		}
		second_couple_obj = {
			"boy":second_couple.boy,
			"girl":second_couple.girl,
			"year":second_couple.year,
			"score":round(second_couple.score,2),
			"appeared":second_couple.appeared,
			"id":second_couple.id,
		}
		JSONObj = {
			"first":first_couple_obj,
			"second":second_couple_obj,
		}
		return HttpResponse(json.dumps(JSONObj),content_type="application/json")
	return HttpResponse("404 Not Found")

def rateCouple(request):
	if request.method == "POST":
		me = Hit.objects.get(seeker='sanket')
		me.hits += 1
		me.save()
		print(get_client_ip(request))
		first_couple_id = int(request.POST.get("first"))
		second_couple_id = int(request.POST.get("second"))
		wins = int(request.POST.get("wins"))
		first_couple = Couple.objects.get(id=first_couple_id)
		second_couple = Couple.objects.get(id=second_couple_id)
		first_score = first_couple.score
		second_score = second_couple.score
		first_rating = math.pow(10,first_score/400)
		second_rating = math.pow(10,second_score/400)
		expectation_first = first_rating/(first_rating+second_rating)
		expectation_second = second_rating/(first_rating+second_rating)
		# When Wins
		s_first = 0
		s_second = 0
		if wins == 1:
			s_first = 1
		elif wins == 2:
			s_second = 1
		else:
			data = {
				"error":"Error Occured"
			}
			return HttpResponse(json.dumps(data),content_type="application/json")
		k_constant = 48
		first_score = first_score + k_constant*(s_first - expectation_first)
		second_score = second_score + k_constant*(s_second - expectation_second)
		first_couple.score = first_score
		first_couple.save()
		second_couple.score = second_score
		second_couple.save()
		data = {
			"first":round(first_score,2),
			"second":round(second_score,2),
			"change":"+" + str(abs(round(k_constant*(s_first - expectation_first),2))),
		}

		# GET COUPLE
		if request.POST.get("year") != 'None':
			year = request.POST.get("year")
			if year == 'first':
				yearInt = 2017
			elif year == 'second':
				yearInt = 2016
			elif year == 'third':
				yearInt = 2015
			elif year == 'fourth':
				yearInt = 2014
			else:
				return HttpResponse("No Such Year")
			random_student_one = randint(0,len(Couple.objects.filter(year=yearInt))-1)
			first_couple = Couple.objects.filter(year=yearInt)[random_student_one]
			while(True):
				random_student_two = randint(0,len(Couple.objects.filter(year=yearInt))-1)
				if random_student_two != random_student_one:
					second_couple = Couple.objects.filter(year=yearInt)[random_student_two]
					break;
				time.sleep(0.5)
		else:
			random_student_one = randint(0,len(Couple.objects.all())-1)
			first_couple = Couple.objects.all()[random_student_one]
			while(True):
				random_student_two = randint(0,len(Couple.objects.all())-1)
				if random_student_two != random_student_one:
					second_couple = Couple.objects.all()[random_student_two]
					break;
				time.sleep(1)


		first_couple_obj = {
			"boy":first_couple.boy,
			"girl":first_couple.girl,
			"year":first_couple.year,
			"score":round(first_couple.score,2),
			"appeared":first_couple.appeared,
			"id":first_couple.id,
		}
		second_couple_obj = {
			"boy":second_couple.boy,
			"girl":second_couple.girl,
			"year":second_couple.year,
			"score":round(second_couple.score,2),
			"appeared":second_couple.appeared,
			"id":second_couple.id,
		}
		JSONObj = {
			"first":first_couple_obj,
			"second":second_couple_obj,
			"instance":data,
		}
		first_couple.appeared = first_couple.appeared + 1
		first_couple.save()
		second_couple.appeared = second_couple.appeared + 1
		second_couple.save()
		return HttpResponse(json.dumps(JSONObj),content_type="application/json")
	return HttpResponse("404 Not Found")

def hitsUpdate(request):
	me = Hit.objects.get(seeker='sanket')
	first = me.hits
	while(True):
		me = Hit.objects.get(seeker="sanket")
		second = me.hits
		if first != second:
			JSONObj = {
				"hits":second,
			}
			break;
		time.sleep(2)

	return HttpResponse(json.dumps(JSONObj),content_type="application/json")

def getList(request):
	if request.method == 'POST':
		me = Hit.objects.get(seeker='sanket')
		me.hits += 1
		me.save()
		year = request.POST.get("year")
		if year == 'first':
			yearInt = 2017
		elif year == 'second':
			yearInt = 2016
		elif year == 'third':
			yearInt = 2015
		elif year == 'fourth':
			yearInt = 2014
		else:
			return HttpResponse("No Such Year")
		male_stud = Student.objects.filter(gender='Male',year=yearInt)
		female_stud = Student.objects.filter(gender='Female',year=yearInt)
		male_list = [stud.name for stud in male_stud]
		female_list = [stud.name for stud in female_stud]
		JSONObj = {
			"males":male_list,
			"females":female_list,
		}
		return HttpResponse(json.dumps(JSONObj),content_type="application/json")
	return HttpResponse("404")

def uniques(request):
	couples = Couple.objects.all()
	arr = []
	for couple in couples:
		dummy = Couple.objects.filter(girl=couple.girl)
		dummy_len = len(dummy)
		if couple.boy == dummy[0].boy:
			arr.append("*" + dummy[0].girl + " and " + dummy[0].boy + "* >> _" + str(dummy[0].score) + "_")

	return render(request, "unique.html", {"couples":arr})

def specificNewCouple(request):
	if request.method == 'POST':
		me = Hit.objects.get(seeker='sanket')
		me.hits += 1
		me.save()
		boy = request.POST.get("boy")
		girl = request.POST.get("girl")
		year = request.POST.get("year")
		JSONObj = {
			"posted":False,
		}
		try:
			couple = Couple.objects.get(boy=boy,girl=girl)
		except:
			yearInt = 0
			if year == 'first':
				yearInt = 2017
			elif year == 'second':
				yearInt = 2016
			elif year == 'third':
				yearInt = 2015
			elif year == 'fourth':
				yearInt = 2014
			else:
				return HttpResponse("No Such Year")
			JSONObj["posted"] = True
			Couple.objects.create(boy=boy,girl=girl,year=yearInt)
			msg = "New Couple Created, " + boy + " and " + girl + " are in love.";
			user = User.objects.get(username="snookie")
			Message.objects.create(user=user,message=msg)
			Message.objects.create(user=user,message="Sweet Prom Night to you both!")
		JSONObj_str = json.dumps(JSONObj)
		return HttpResponse(JSONObj_str,content_type="application/json")
	return HttpResponse("404 Not Found")
def randomNewCouple(request):
	if request.method == 'POST':
		year = request.POST.get("year")
		yearInt = 0
		if year == 'first':
			yearInt = 2017
		elif year == 'second':
			yearInt = 2016
		elif year == 'third':
			yearInt = 2015
		elif year == 'fourth':
			yearInt = 2014
		else:
			return HttpResponse("No Such Year")
		boy_list = Student.objects.filter(gender="Male",year=yearInt)
		girl_list = Student.objects.filter(gender="Female",year=yearInt)
		boy = boy_list[randint(0,len(boy_list)-1)]
		girl = girl_list[randint(0,len(girl_list)-1)]
		JSONObj = {
			"boy":boy.name,
			"girl":girl.name,
			"year":yearInt,
		}
		while(True):
			boy = boy_list[randint(0,len(boy_list)-1)]
			girl = girl_list[randint(0,len(girl_list)-1)]
			try:
				Couple.objects.get(boy=boy,girl=girl,year=yearInt)
			except:
				break;
			time.sleep(1)

		JSONObj = {
			"boy":boy.name,
			"girl":girl.name,
			"year":yearInt,
		}
		msg = "By Fluke, " + boy.name + " and " + girl.name + " get matched."
		user = User.objects.get(username='snookie')
		Message.objects.create(user=user,message=msg)
		Couple.objects.create(boy=boy,girl=girl,year=yearInt)
		JSONObj_str = json.dumps(JSONObj)
		return HttpResponse(JSONObj_str,content_type="application/json")
	return HttpResponse("404 Not Found")
def newCouple(request):
	return HttpResponse("Hey Smart Boy, You are at a Wrong Place")

def chatUpdate(request):
	if request.method == "POST":
		if request.POST.get("action") == "postmsg":
			sender = request.POST.get("sender")
			msg = request.POST.get("message")
			user = User.objects.get(username=sender)
			Message.objects.create(user=user,message=msg)
		elif request.POST.get("action") == "updatechat":
			initial = Message.objects.all()
			latest = initial
			while(True):
				latest = Message.objects.all()
				if len(latest) != len(initial):
					JSONObj = [{
						"sender":str(obj.user.username),
						"message":str(obj.message),
						} for obj in latest[len(initial):len(latest)]]
					break;
				time.sleep(1)
			return HttpResponse(json.dumps(JSONObj),content_type="application/json")
	return HttpResponse("I knew were trouble")

def userWorks(request):
	if request.method == 'POST':
		if request.POST.get("action") == "authenticate":
			firstname = request.POST.get("firstname")
			lastname = request.POST.get("lastname")
			username = firstname + lastname
			username = username.lower()
			raw_password = request.POST.get("password")

			user = authenticate(username=username, password=raw_password)
			JSONObj = {
				"authenticated":False,
			}
			if user is not None:
				login(request, user)
				JSONObj["authenticated"] = True
				return HttpResponse(json.dumps(JSONObj),content_type="application/json")
			return HttpResponse(json.dumps(JSONObj),content_type="application/json")

		elif request.POST.get("action") == "create_user":
			firstname = request.POST.get("firstname")
			lastname = request.POST.get("lastname")
			raw_password = request.POST.get("password")
			username = firstname + lastname
			username = username.lower()
			JSONObj = {
				"status":"None"
			}
			try:
				User.objects.get(username=username)
				JSONObj['status'] = "userexist"
			except:
				User.objects.create_user(username=username,password=raw_password)
				user = User.objects.get(username=username)
				user.first_name = firstname 
				user.last_name = lastname
				user.save()
				user = authenticate(username=username,password=raw_password)
				login(request,user)
				JSONObj['status'] = "usercreated"
			return HttpResponse(json.dumps(JSONObj),content_type="application/json")

	return HttpResponse("Welcome")

def something(request,keyword=None):
	return HttpResponse(keyword)

def countHits(request):
	me = Hit.objects.get(seeker='sanket')
	return render(request, "hits.html", {"hits":me.hits})


def refresh(request,what=None):
	if what == 'firstname':
		r = requests.get("https://gist.githubusercontent.com/sanket143/0aeb677e335a179f3c5777abb262c036/raw/1782492f8b92a107120ac155470045378718936a/firstname.json")
		json_str = r.content
		json_arr = json.loads(json_str)
		for name in json_arr:
			try:
				FirstName.objects.get(name=name)
			except:
				FirstName.objects.create(name=name)
		return HttpResponse("First Name Updated")
	else:
		r = requests.get("https://gist.githubusercontent.com/sanket143/4951be74131184df3bc0eda4feffbec1/raw/a0483f2827dabf2706f2db54beeab356d17a1219/surname.json")
		json_str = r.content
		json_arr = json.loads(json_str)
		for name in json_arr:
			try:
				Surname.objects.get(surname=name)
			except:
				Surname.objects.create(surname=name)
		return HttpResponse("Surname Updated")
	links = [
		"https://gist.githubusercontent.com/sanket143/7d4299d67dc9beeb200c57d0d7d9c75a/raw/b1475cbae552c0886bc7344daf2c4b10c912bf6d/1YearGirls.json",
		"https://gist.githubusercontent.com/sanket143/30eda8779d9e61b04ba8d84f2b19b8e7/raw/0da2f046dc90aedf6ddf8a9925f4a1018471b5ed/1YearBoys.json",
		"https://gist.githubusercontent.com/sanket143/077a0f9bf91c1bb9fc97bdcb57d685a8/raw/b7f53c8f7d006615844e7407f95833b6e0226fb7/2YearGirls.json",
		"https://gist.githubusercontent.com/sanket143/dcf9887f973c2eb180dc6ddd3f5cb441/raw/12606a8d482513eb31202034e6f2df0ca6d58368/2YearBoys.json",
		"https://gist.githubusercontent.com/sanket143/60f86576b0d7a574e029af3753cd8fe0/raw/9fa012c54d2d01a331a599a0b9fa27f0748d2247/3YearGirls.json",
		"https://gist.githubusercontent.com/sanket143/d4adc34deae3a11867e1f10f13929c18/raw/531ad6f34138afc9906b239943cf29f443406a3b/3YearBoys.json",
		"https://gist.githubusercontent.com/sanket143/e101839cc19bb647ef3be7db9370ba15/raw/b4c2dc983c1335c096706378ed09c425ed28284f/4YearGirls.json",
		"https://gist.githubusercontent.com/sanket143/bc8022430f6458ef8285ac2ab4ddaf8a/raw/bbed24821db199d84ef7264af3568ea9aaa9ff38/4YearBoys.json",
	]
	JSONObj = []
	for sr in range(len(links)):
		r = requests.get(links[sr])
		json_str = r.content
		json_arr = json.loads(json_str)
		json_obj = {
			"names":json_arr,
			"gender":None,
			"year":None,
			"yearInt":0,
		}
		if sr%2 == 0:
			json_obj['gender'] = "Female"
		else:
			json_obj['gender'] = "Male"
		if math.floor(sr/2) == 0:
			json_obj['year'] = "First"
			json_obj['yearInt'] = 2017
		elif math.floor(sr/2) == 1:
			json_obj['year'] = "Second"
			json_obj['yearInt'] = 2016
		elif math.floor(sr/2) == 2:
			json_obj['year'] = "Third"
			json_obj['yearInt'] = 2015
		elif math.floor(sr/2) == 3:
			json_obj['year'] = "Fourth"
			json_obj['yearInt'] = 2014
		JSONObj.append(json_obj)

	for StudentObj in JSONObj:
		for name in StudentObj["names"]:
			try:
				stud = Student.objects.get(name=name.title(),year=StudentObj['yearInt'])
			except:
				stud = Student.objects.create(name=name.title(),gender=StudentObj['gender'])
				stud.year = StudentObj['yearInt']
				stud.save()
	return render(request,"refresh.html",{"StudentArr":JSONObj})

