from django.contrib import admin
from .models import Student, Couple, FirstName, Surname, Message, Hit, UserInfo
# Register your models here.
admin.site.register(Student)
admin.site.register(Couple)
admin.site.register(FirstName)
admin.site.register(Surname)
admin.site.register(Message)
admin.site.register(Hit)
admin.site.register(UserInfo)